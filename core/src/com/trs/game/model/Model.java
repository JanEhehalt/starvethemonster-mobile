package com.trs.game.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.trs.game.StaticMath;

import java.util.ArrayList;

public class Model {

    final int WALL_LIFETIME = 40;

    private Monster monster;
    private ArrayList<Wall> walls;
    private ArrayList<Projectile> projectiles;
    private Polygon tempPolygon;
    private Vector2 tempStart;
    private boolean drawing = false;
    private int currentLength;

    private boolean toReset;
    private boolean ending;

    private int difficulty;


    public Model(int difficulty){
        this.difficulty = difficulty;

        monster = new Monster(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2, 5f/3f*difficulty);
        walls = new ArrayList<>();
        projectiles = new ArrayList<>();
    }

    public void timerStep() {
        monster.move(walls, projectiles);

        for(int i = projectiles.size() - 1; i >= 0; i--){
            Projectile projectile = projectiles.get(i);
            projectile.move(walls);
            if(projectile.getxPos() < -(Gdx.graphics.getWidth()*0.1) || projectile.getxPos() > Gdx.graphics.getWidth()*1.1 || projectile.getyPos() < -(Gdx.graphics.getHeight()*0.1) || projectile.getyPos() > Gdx.graphics.getHeight()*1.1){
                projectiles.remove(i);
            }
        }
        for (int i = 0; i < walls.size(); i++) {
            walls.get(i).timerStep();
            if (walls.get(i).getLifetime() == 0) {
                walls.remove(i);
                i--;
            }
        }
        if (monster.getIsDead()) {
            toReset = true;
            ending = false; // Monster win - Player lost
        }
        if(monster.getHp() == monster.getMaxHp()){
            toReset = true;
            ending = true;
        }

        // Generation of new projectiles
        int value = (int) (Math.random() * 15);
        if(value < (4)){
            projectiles.add(spawnProjectile());
        }
    }
    
    public void startWall(int x, int y){
        if(!drawing){
            tempPolygon = StaticMath.createPolygon(x,y,0,10,5);
            tempStart = new Vector2(x,y);
            drawing = true;
        }
    }
    public void adjustWall(int x, int y){
        if(drawing){
            double angle = StaticMath.calculateAngle(x,y,(int)tempStart.x,(int)tempStart.y);
            tempPolygon = StaticMath.createPolygon((int)tempStart.x, (int)tempStart.y, angle-Math.PI,10,Vector2.dst(tempStart.x,tempStart.y,x,y));
            currentLength = (int)Vector2.dst(tempStart.x,tempStart.y,x,y);
        }
    }
    public void finishWall(int x, int y){
        if(Vector2.dst(tempStart.x,tempStart.y,x,y) > 50) {
            double angle = StaticMath.calculateAngle(x,y,(int)tempStart.x,(int)tempStart.y);
            tempPolygon = StaticMath.createPolygon((int)tempStart.x, (int)tempStart.y, angle-Math.PI,10,Vector2.dst(tempStart.x,tempStart.y,x,y));

            boolean possible = true;
            for(Wall wall : walls){
                if(Intersector.overlapConvexPolygons(tempPolygon, wall.getPolygon())){
                    possible = false;
                    break;
                }
            }

            if(Intersector.overlapConvexPolygons(tempPolygon, monster.getMonsterPolygon())){
                possible = false;
            }
            /*
            for(Projectile projectile : projectiles){
                if(Intersector.overlapConvexPolygons(tempPolygon,projectile.getPolygon())){
                    possible = false;
                    break;
                }
            }
            */

            if(possible){
                TempWall newWall = new TempWall(angle-Math.PI, tempPolygon, WALL_LIFETIME);
                for(int i = 0; i < projectiles.size(); i++){
                    if(Intersector.overlapConvexPolygons(projectiles.get(i).getPolygon(),newWall.getPolygon())){
                        projectiles.remove(i);
                        i--;
                    }
                }
                walls.add(newWall);
            }
        }
        tempPolygon = null;
        tempStart = null;
        drawing = false;
        currentLength = 0;
    }

    private Projectile spawnProjectile(){
        int xPos;
        int yPos;

        int direction = (int) (Math.random() * 4);

        // up
        if(direction == 0){
            xPos = (int) (Math.random() * Gdx.graphics.getWidth());
            yPos = (int)(Gdx.graphics.getHeight()*1.05f);
        }
        // right
        else if(direction == 1){
            xPos = (int)(Gdx.graphics.getWidth()*1.05f);
            yPos = (int) (Math.random() * Gdx.graphics.getHeight());
        }
        // down
        else if(direction == 2){
            xPos = (int) (Math.random() * Gdx.graphics.getWidth());
            yPos = -(int)(Gdx.graphics.getHeight()*0.05f);
        }
        // left
        else{
            xPos = -(int)(Gdx.graphics.getWidth()*0.05f);;
            yPos = (int) (Math.random() * Gdx.graphics.getHeight());
        }

        return new Projectile(xPos, yPos, monster.getxPos(), monster.getyPos(), difficulty*1.8);
    }

    public ArrayList<Wall> getWalls(){
        return walls;
    }

    public Monster getMonster(){
        return monster;
    }

    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    public Polygon getTempPolygon() {
        return tempPolygon;
    }


    public int getCurrentLength() {
        return currentLength;
    }

    public boolean isToReset() {
        return toReset;
    }

    public boolean getEnding() {
        return ending;
    }
}
