package com.trs.game.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.trs.game.StaticMath;

import java.util.ArrayList;

public class Monster {

    private double SPEED;
    private final int WIDTH = 120;
    private final int HEIGHT = 120;

    private int xPos;
    private int yPos;
    private double movementX;
    private double movementY;
    private int xPosTarget;
    private int yPosTarget;
    private int hp;
    private int maxHp;
    private boolean isDead;

    public Monster(int xPos, int yPos, double SPEED){
        this.xPos = xPos;
        this.yPos = yPos;
        this.maxHp = 700;
        this.hp = 400;
        isDead = false;
        this.SPEED = SPEED;

        generateNewTarget();
    }

    public void drawMonster(ShapeRenderer renderer, PolygonSpriteBatch polygonSpriteBatch, SpriteBatch batch, BitmapFont font){
        if(renderer.isDrawing()) renderer.end();
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        //BODY
        renderer.setColor(Color.BLACK);
        renderer.rect(xPos, yPos, WIDTH,HEIGHT);
        //EYES
        renderer.setColor(Color.RED);
        renderer.rect(xPos + 10f/50f*WIDTH, yPos + 30f/50f*HEIGHT, 5f/50f*WIDTH, 10f/50f*HEIGHT);
        renderer.rect(xPos + 35f/50f*WIDTH, yPos + 30f/50f*HEIGHT, 5f/50f*WIDTH, 10f/50f*HEIGHT);
        //MOUTH
        renderer.setColor(Color.WHITE);
        renderer.rect(xPos + 10f/50f*WIDTH, yPos + 10f/50f*HEIGHT, 30f/50f*WIDTH, 10f/50f*HEIGHT);
        //TEETH
        renderer.setColor(Color.RED);
        // TOP
        renderer.triangle(xPos+10f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+10f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+13f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        renderer.triangle(xPos+13f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+16f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+19f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        renderer.triangle(xPos+19f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+22f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+25f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        renderer.triangle(xPos+25f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+28f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+31f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        renderer.triangle(xPos+31f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+34f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+37f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        renderer.triangle(xPos+37f/50f*WIDTH,yPos+20f/50f*HEIGHT,xPos+40f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+40f/50f*WIDTH,yPos+20f/50f*HEIGHT);
        // BOTTOM
        renderer.triangle(xPos+10f/50f*WIDTH,yPos+10f/50f*HEIGHT,xPos+13f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+16f/50f*WIDTH,yPos+10f/50f*HEIGHT);
        renderer.triangle(xPos+16f/50f*WIDTH,yPos+10f/50f*HEIGHT,xPos+19f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+22f/50f*WIDTH,yPos+10f/50f*HEIGHT);
        renderer.triangle(xPos+22f/50f*WIDTH,yPos+10f/50f*HEIGHT,xPos+25f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+28f/50f*WIDTH,yPos+10f/50f*HEIGHT);
        renderer.triangle(xPos+28f/50f*WIDTH,yPos+10f/50f*HEIGHT,xPos+31f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+34f/50f*WIDTH,yPos+10f/50f*HEIGHT);
        renderer.triangle(xPos+34f/50f*WIDTH,yPos+10f/50f*HEIGHT,xPos+37f/50f*WIDTH,yPos+15f/50f*HEIGHT,xPos+40f/50f*WIDTH,yPos+10f/50f*HEIGHT);

        // HUNGER BAR
        renderer.setColor(Color.BLACK);
        renderer.rect(xPos - 0.25f*2*WIDTH-1, yPos + 55f/50f*HEIGHT, 2*WIDTH+2,22);
        renderer.setColor(Color.BROWN);
        renderer.rect(xPos - 0.25f*2*WIDTH, yPos + 56f/50f*HEIGHT, 2*WIDTH * (float)hp/(float)maxHp,20);
        //renderer.setColor(Color.DARK_GRAY);
        //renderer.rect(xPos - 25 + 100f * (float)hp/(float)maxHp, yPos + 56, 100 - 100f * (float)hp/(float)maxHp,20);

        renderer.end();
/*
        batch.begin();
        font.getData().setScale(0.8f);
        font.setColor(Color.DARK_GRAY);
        font.draw(batch, "Saturation",xPos - 0.25f*2*WIDTH+5, yPos + 71f/50f*HEIGHT-HEIGHT/10f);
        font.getData().setScale(1f);
        font.setColor(Color.BLACK);
        batch.end();
*/

    }

    public void move(ArrayList<Wall> walls, ArrayList<Projectile> projectiles){
        checkTarget();

        this.xPos += this.movementX;
        this.yPos += this.movementY;

        // Collisions
        Polygon monsterPolygon = getMonsterPolygon();

        hp-=1;

        for (int i = projectiles.size() - 1; i >= 0; i--) {
            Projectile projectile = projectiles.get(i);
            if (Intersector.overlapConvexPolygons(monsterPolygon, projectile.getPolygon())) {
                projectiles.remove(i);
                hit();
            }
        }
        if(hp <= 0){
            die();
        }

        for(int i = walls.size() - 1; i >= 0; i--){
            Polygon wall = walls.get(i).getPolygon();
            if(Intersector.overlapConvexPolygons(monsterPolygon, wall)){
                setxPos(getxPos() - (int) movementX);
                setyPos(getyPos() - (int) movementY);

                generateNewTarget();
            }
        }
    }

    private void hit(){
        hp += 20;
        if(hp > maxHp){
            hp = maxHp;
        }
    }

    public Polygon getMonsterPolygon(){
        float[] verticesMonster = new float[8];
        verticesMonster[0] = xPos;
        verticesMonster[1] = yPos;
        verticesMonster[2] = xPos + WIDTH;
        verticesMonster[3] = yPos;
        verticesMonster[4] = xPos + WIDTH;
        verticesMonster[5] = yPos + HEIGHT;
        verticesMonster[6] = xPos;
        verticesMonster[7] = yPos + HEIGHT;
        return new Polygon(verticesMonster);
    }

    private void checkTarget(){
        if(this.xPos == this.xPosTarget && this.yPos == this.yPosTarget){
            this.generateNewTarget();
        }
        if(Math.random() >= 0.95){
            this.generateNewTarget();
        }
    }

    private void generateNewTarget(){
        xPosTarget = (int) (Math.random() * 1400) + 100;
        yPosTarget = (int) (Math.random() * 700) + 100;

        calculateMovement();
    }

    private void calculateMovement(){
        double angle = StaticMath.calculateAngle(this.xPos, this.yPos, this.xPosTarget, this.yPosTarget);

        movementX = Math.cos(angle) * SPEED;
        movementY = Math.sin(angle) * SPEED;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    private void die(){
        this.isDead = true;
    }

    public boolean getIsDead(){
        return this.isDead;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
}
